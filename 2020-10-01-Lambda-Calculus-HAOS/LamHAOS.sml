datatype hlam = HV of string
	       |HA of hlam * hlam
	       |HL of hlam -> hlam

datatype lam = V of string
	      |A of lam * lam
	      |L of string * lam

(* Q1 *)
fun subst (x,e) (HV(a)) = if (x=a) then e
			  else HV(a)
  | subst (x,e) (HA(a,b)) = HA (subst (x,e) (a), subst (x,e) (b))
  | subst (x,e) (HL(f))  = let fun fp t = subst(x,e) (f t)
			   in
			       HL(fp)
			   end

(* Q2 *)
fun abstract x (M:hlam) = let fun f n:hlam = case M of
						 HV(e) =>subst(x,n) (HV(e))
					       | HA(a,b) => subst(x,n) (HA(a,b))
					       | HL(f) => subst(x,n) (HL(f))
			  in
			      HL(f)
			  end

(* Q3 *)
fun remove [] = []
  | remove (x::xs) = x::remove(List.filter(fn y => y<>x) xs)

fun helper (xs,s) = case xs of
			[] => !s
		      | y::ys  => if String.compare(!s,y)<>EQUAL then helper(ys,s)
				  else (s:= !s^"z";helper(ys,s))

fun fresh xs = let
    val s = ref "a";
in
    helper (xs,s)
end

fun freeP (HV(e)) = [e]
  | freeP (HA(a,b)) = remove(freeP(a) @ freeP(b))

			     
fun freshen M = fresh(freeP(M))
		     

(* Q4 *)
fun hoas (V(e)) = HV(e)
  | hoas (A(a,b)) = HA(hoas a,hoas b)
  | hoas (L(s,e)) = abstract s (hoas e)
