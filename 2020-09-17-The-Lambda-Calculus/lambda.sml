(* Q1 *)
datatype expr = NIL
	      | var of string
	      | app of expr*expr
	      | abst of string*expr


(* Q2 *)

fun freshhelp (xs,fins,pos) = case xs of
				  [] => !fins
				| y::ys => (pos := !pos+1 ;(
					     case String.sub( String.map Char.toUpper y, !pos) of
						  #"Z" => (fins := !fins^String.str(#"A")^freshhelp (ys,fins,pos); !fins) 
						| _ => ( fins := !fins^(String.str(Char.succ(String.sub(String.map Char.toUpper y, !pos ))))^freshhelp(ys,fins,pos);!fins) ))

fun fresh xs = let
    val pos = ref ~1
    val fins = ref "";
in
    freshhelp (xs,fins,pos)
end

(* Q3 *)

fun bound (x,[])=false
  | bound (x,y::ys) = if x = y then true else bound (x,ys)

fun dif ([],xs) = []
  | dif (xs,[]) = xs
  | dif (x::xs,ys) = if bound(x,ys) then dif(xs,ys) else x::dif(xs,ys)

fun free1 ls (abst(x,xs))= dif((free1 ls xs),[x])
  | free1 ls (app(xs,ys)) = free1 ls xs @ free1 ls ys
  | free1 ls (var(x))  = if (bound(x,ls) = true) then [] else [x]
  | free1 ls NIL = ls


(* Q4 *)

fun subst (y,N) (var(x)) = if (x = y) then N else (var(x))
  | subst (y,N) (abst(a,b)) = if (y = a) then (abst(a,b)) else (abst(a,(subst (y,N) b )))
  | subst (y,N) (app(a,b)) = app((subst (y,N) a), (subst (y,N) b))
  | subst ("",N) e1 = e1
  | subst (y,NIL) e1 = e1
  | subst (y,N) NIL = NIL
