(* Q1 *)
fun foldr sfun s0 [] = s0 
  | foldr sfun s0 (x::xs) = sfun(x,(foldr sfun s0 xs))

fun foldl sfun s0 []= s0
  | foldl sfun s0 (x::xs) = foldl sfun (sfun (x,s0)) xs

(* Q2 *)
fun sum xs = foldl op+ 0 xs

(* Q3 a*)		   
fun partition f l = foldr ( fn(x,xs) => if (f x) then (x::(#1(xs)), #2(xs)) else (#1(xs), x::(#2(xs)))) ([],[]) l

(* Q3 b*)
fun map f xs = foldl (fn (x,xs) => f(x)::xs) [] xs

(* Q3 c*)		     
fun reverse xs = foldl (op ::) [] xs
	
(* Q3 d*)
datatype 'a Find = LookingFor of int
		  |Found of 'a
				
fun nth xs n =
    let
	fun next (x,(LookingFor n)) =
	    if n=1 then Found x
	    else LookingFor(n-1)
	  | next (_,Found x) = Found x
	fun optn (Found n) = SOME n
	  | optn _ = NONE
    in
	optn(foldl next (LookingFor n) xs)
    end
	
