signature SIG = sig
    type symbol
    val arity:symbol->int
			  
structure Ord:ORD_KEY where type ord_key = symbol	
	end

signature VAR = sig
    type var
    structure ORD:ORD_KEY where type ord_key = var			
end


functor Term (structure S:SIG;structure V:VAR) = struct
datatype term = constant of S.symbol
	     |var of V.var
	     |fun_sym of S.symbol*term list				   


fun occurs (constant(a),b:V.var)= false
  | occurs (var(a),b:V.var) = if V.ORD.compare(a,b) = EQUAL then true else false
  | occurs (fun_sym(a,[]),b:V.var) = false 
  | occurs (fun_sym(a,c::cl),b:V.var) = case occurs(c,b) of
					   true =>  true
					 | false => (case cl of [] => false
							       |d::dl => occurs(d,b))
end
 
functor Telescope (structure S:SIG; structure V:VAR) = struct
structure terms = Term(structure S = S; structure V = V)
structure Map = RedBlackMapFn(V.ORD)
type telescope = terms.term Map.map

fun helper (v:V.var,terms.constant s,t:telescope) = true
  | helper (v:V.var,terms.var s,t:telescope) = if V.ORD.compare(v,s) = EQUAL then true
					       else (case Map.find(t,s) of
							 SOME option => if terms.occurs(option,v) = false andalso helper(v,option,t) = true then true else false
						       | NONE => true)
  | helper (v:V.var,terms.fun_sym(s,[]),t:telescope) = true
  | helper (v:V.var,terms.fun_sym(s,x::xs),t:telescope)  = if helper(v,x,t) = true andalso helper(v,terms.fun_sym(s,xs),t) = true then true else false

fun insert (t:telescope,v:V.var,tm:terms.term) = if helper(v,tm,t) = true then Map.insert(t,v,tm) else t

fun sub (t:telescope,terms.constant s) = terms.constant s
  | sub (t:telescope,terms.var s) = (case Map.find(t,s) of
					 SOME option => sub(t,option)
				       | NONE => terms.var s)
  | sub (t:telescope,terms.fun_sym(s,x)) = let fun help(t,[]) = []
						 | help (t,x::xs) = sub(t,x)::help(t,xs)
					   in
					       terms.fun_sym(s,help(t,x))
					   end


fun unifyL (t:telescope,[],[]) = true
  | unifyL (t,x::xs,y::ys) = let fun unify(t:telescope,(terms.constant a,b:terms.term)) = (case b of terms.var(b) => if helper(b,terms.constant a,t)=true then true else false
											       |  _ =>false )
				  | unify (t:telescope,(terms.var a,b:terms.term )) = if helper(a,b,t) = true then true else false
			    in
				if unify(t,(x,y)) = true then unifyL(t,xs,ys) else false
			    end

fun ins (t:telescope,l::[],x::[]) = (case l of terms.constant(l)=>(case x of terms.var(x) => SOME(Map.insert(t,x,terms.constant l))
									|  _ => NONE )
					   | terms.var(l) => (case x of terms.constant(x) => SOME(Map.insert(t,l,terms.constant x))
								      | _ => SOME(Map.insert(t,l,sub(t,x)))))
							  
(* Q1 *)
fun unify (t:telescope,(terms.constant a,b:terms.term)):telescope option = (case b of terms.var(b) => if helper(b,terms.constant a,t) = true then SOME(Map.insert(t,b,terms.constant a)) else NONE
										 |  _ => NONE )
  | unify (t:telescope,(terms.var a,b:terms.term )) = if helper(a,b,t) = true then SOME(Map.insert(t,a,sub(t,b))) else NONE
  | unify (t:telescope,(terms.fun_sym(f,[]),g:terms.term)) = (case g of terms.var(g) => unify (t,(terms.var g,terms.fun_sym(f,[])))
								      |_ => NONE)
  | unify  (t:telescope,(terms.fun_sym(f,l::ls),g:terms.term)) = (case g of terms.constant(g) => NONE
							      | terms.var(g) => unify (t,(terms.var g,terms.fun_sym(f,l::ls)))
							      | terms.fun_sym(g,[]) => NONE
							      | terms.fun_sym(g,x::xs) => if S.arity(f)<>S.arity(g) then NONE else
											  (case unifyL(t,l::ls,x::xs) of false => NONE
														       | true  => ins(t,l::ls,x::xs)))

(* Q2 *)
fun unifyList (t:telescope,l:(terms.term*terms.term)list)= (case l of
							       [] => NONE	       
							      |x::[] => unify(t,(#1(x),#2(x)))		   
							      |x =>let val i = unify(t,(#1(List.nth(x,0)),#2(List.nth(x,0))))
							      in
								  unifyList (t,x)
							      end)
end
