(* Q1 *)

signature COUNTER = sig
    val x : int ref		
    val incr: unit -> unit			  
    val decrement: unit -> unit			       
    val get: unit -> int			 
end
			
structure Counter : COUNTER
= struct
              val x = ref 0		 
              fun incr () = x := !x + 1					  
	      fun decrement () = x := !x - 1				 
	      fun get () = !x		       
end

(* Q2 *)
      
functor MkCounter () : COUNTER = struct
val x = ref 0
fun incr () =  x := !x + 1	
fun decrement () =  x := !x - 1	
fun get () = !x
end
