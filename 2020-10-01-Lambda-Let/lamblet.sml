type Var = string

datatype expr = var of Var
	       |app of expr * expr
	       |abst of Var * expr

(* Q1 *)
	
datatype Llet = Let of (Var * expr) * expr
datatype Lletrec = Letrec of (Var * expr)list * expr

(* Q2 *)
						   				
fun unletrec (Letrec(((v:Var,e1:expr)::lst),e2)) = case lst of
					    [] => Let((v,e1),e2)
					   | l::ls => unletrec (Letrec(((app(var(v),e1:expr),app(l))::ls),e2))

fun unlet (Let((v,e1),e2)) = app(abst(v,e2),e1)
				
