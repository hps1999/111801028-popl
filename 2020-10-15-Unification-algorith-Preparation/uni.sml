(* Q1 *)
signature SIG=sig
    type symbol
    val arity:symbol->int
structure Ord:ORD_KEY where type ord_key=symbol	
end

(* Q2 *)
signature VAR=sig
    type var
    structure ORD:ORD_KEY where type ord_key=var			
end

(* Q3 *)
functor Term (structure S:SIG;structure V:VAR)=struct
datatype term=constant of S.symbol
	     |var of V.var
	     |funsym of S.symbol*term list				   

(* Q4 *)
fun occurs(constant(a),b:V.var)=false
  | occurs (var(a),b:V.var) =if V.ORD.compare(a,b)=EQUAL then true else false
  | occurs (funsym(a,[]),b:V.var) =false 
  | occurs (funsym(a,c::cl),b:V.var) =case occurs(c,b) of
					   true=>  true
					 | false =>(case cl of
						       []=>false
						       |d::dl=>occurs(d,b)
								     )
end
