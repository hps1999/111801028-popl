(* 
curry: ('a*'b*'c->'d) -> 'a->'b->'c->'d
uncurry: 'a->'b->'c->'d -> ('a*'b*'c->'d)
*)
fun curry f x y z = f(x,y,z)
fun uncurry f (x,y,z) = f x y z

fun fst (a,b) = a
fun snd (a,b) = b

fun length nil=0 
  | length(x::xs) = 1 + length(xs)

fun reverse xs =
    let
	fun revhelp (xs1,rev) =
	    case xs1 of
		[] => rev
        | (x::xs2) => revhelp(xs2, x::rev)
    in
	revhelp(xs,[])
end

fun fib n =
    let
	fun fibhelp (a,b,n1) =
	    if n1 = 1 then 0
	    else if n1 = 2 then 1
	    else a+fibhelp(b,a+b,n1-1)
    in
	fibhelp(0,1,n)
end
