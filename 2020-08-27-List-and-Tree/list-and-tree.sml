(* Q1 *)
fun map f [] = []
  | map f(x::xs) = f x:: map f(xs)

(* Q2 *)
datatype 'a tree = nil		       
		 | node of 'a tree * 'a * 'a tree

(* Q3 *)
(* ('a -> 'b) -> 'a tree -> 'b tree *)					     
fun treemap f nil = nil
  | treemap f (node(left,n,right)) = node(treemap f(left),f n,treemap f (right))
					 
(* Q4 *)
(* 'a tree -> 'a list *)
fun inorder nil = []
  | inorder (node(left,n,right)) = inorder left @ [n] @ inorder right

(* 'a tree -> 'a list *)								
fun preorder nil = []
  | preorder (node(left,n,right)) = [n] @ preorder left @ preorder right

(* 'a tree -> 'a list *)
fun postorder nil = []
  | postorder (node(left,n,right)) = postorder left @ postorder right @ [n]

(* Q5 *)									    
fun rotate (node(node(left_left,left,left_right),n,right)) = (node(left_left,left,node(left_right,n,right)))
  | rotate treee = treee
